unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, Grids, Spin,
  ExtCtrls, StdCtrls, ComCtrls, TAGraph, TASeries;

type

  { TForm1 }

  TForm1 = class(TForm)
    Chart1: TChart;
    Chart1LineSeries1: TLineSeries;
    Chart1LineSeries2: TLineSeries;
    Chart1LineSeries3: TLineSeries;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    GradCond: TFloatSpinEdit;
    MainMenu1: TMainMenu;
    LoadFile: TMenuItem;
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    SaveFile: TMenuItem;
    GradSize: TSpinEdit;
    SampleRate: TSpinEdit;
    GradCntRow: TSpinEdit;
    AlarmIndication: TShape;
    ObjectTemp: TSpinEdit;
    StaticText1: TStaticText;
    StaticText10: TStaticText;
    StaticText11: TStaticText;
    StaticText12: TStaticText;
    StaticText13: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    StaticText6: TStaticText;
    StaticText7: TStaticText;
    StaticText8: TStaticText;
    StaticText9: TStaticText;
    StringGrid1: TStringGrid;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TempCntRow: TSpinEdit;
    TempCond: TSpinEdit;
    Timer1: TTimer;
    function BitToTemp(BitVal:integer):real;
    procedure LoadFileClick(Sender: TObject);
    procedure IDTresults();
    procedure TermopileResults();
    procedure Timer1Timer(Sender: TObject);
  private

  public
    TermopileFile: boolean;
  end;
const
  GRADCOLSample = 4;
  GRADSampleRate = 5;
  GRADCondition = 6;
  TEMPCondition = 7;

  GRADCOLSample2 = 6;
  GRADSampleRate2 = 7;
  GRADCondition2 = 8;
  TEMPCondition2 = 9;
{ TForm1 }

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

//***********************************************************************************************************
//Temperature calculation for IDT and IDOT
//***********************************************************************************************************
function TForm1.BitToTemp(BitVal:integer):real;
const
	Rp = 3300;         // Ω
	Unap = 3.3;        // V
	a = -0.0000253959668559964;
	b = 0.179746147903927;
	c = -130.677545109844;
	Upr = 0.79;        // V - naměřená hodnota
	RefPNPK = 735;     // referenční hodnota odpovídající Upr
var
	Rt: real;          // Ω
	Ut: real;          // V
	T: real;           // oC
begin
	Ut:=Upr*BitVal/RefPNPK;
	Rt:=Rp*Ut/(Unap-Ut);
	T:=a*Rt*Rt+b*Rt+c;
	Result:=T;
end;

procedure TForm1.LoadFileClick(Sender: TObject);
var
  filename: string;
  i: integer;
begin
    if OpenDialog1.Execute then
    begin
      filename := OpenDialog1.Filename;
      StringGrid1.LoadFromCSVFile(filename,';');
    end;
    if StringGrid1.ColCount = 1 then exit;
    if StringGrid1.RowCount = 1 then exit;
    if StringGrid1.Cells[1,1] = '' then TermopileFile := True
    else TermopileFile := False;
    if TermopileFile = False then IDTresults()
    else TermopileResults();

end;

procedure TForm1.IDTresults();
var i: integer;
begin
    for i := 1 to StringGrid1.RowCount-1 do
    begin
         StringGrid1.Cells[2,i]:=FloatToStr(BitToTemp(StrToInt(StringGrid1.Cells[1,i])));
    end;
    StringGrid1.ColCount:=StringGrid1.ColCount+1;
    StringGrid1.Cells[(StringGrid1.ColCount-1),0]:='Gradient-[deltaAD/SampleCnt]';
    for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
    begin
         StringGrid1.Cells[(StringGrid1.ColCount-1),i]:=FloatToStr((StrToInt(StringGrid1.Cells[1,i])-StrToInt(StringGrid1.Cells[1,i-GradSize.Value]))/GradSize.Value);
    end;
    StringGrid1.ColCount:=StringGrid1.ColCount+1;
    StringGrid1.Cells[(StringGrid1.ColCount-1),0]:='Gradient-[°C/s]';
    for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
    begin
         StringGrid1.Cells[(StringGrid1.ColCount-1),i]:=FloatToStr((StrToFloat(StringGrid1.Cells[2,i])-StrToFloat(StringGrid1.Cells[2,i-GradSize.Value]))/(SampleRate.Value*GradSize.Value));
    end;
    StringGrid1.ColCount:=StringGrid1.ColCount+1;
    StringGrid1.Cells[(StringGrid1.ColCount-1),0]:='AlarmGrad';
    StringGrid1.ColCount:=StringGrid1.ColCount+1;
    StringGrid1.Cells[(StringGrid1.ColCount-1),0]:='AlarmTemp';
end;

procedure TForm1.TermopileResults();
var i: integer;
begin
    StringGrid1.ColCount:=StringGrid1.ColCount+1;
    StringGrid1.Cells[(StringGrid1.ColCount-1),0]:='Gradient-[deltaAD/SampleCnt]';
    for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
    begin
         StringGrid1.Cells[(StringGrid1.ColCount-1),i]:='';
    end;
    StringGrid1.ColCount:=StringGrid1.ColCount+1;
    StringGrid1.Cells[(StringGrid1.ColCount-1),0]:='Gradient-[°C/s]';
    for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
    begin
         StringGrid1.Cells[(StringGrid1.ColCount-1),i]:=FloatToStr((StrToFloat(StringGrid1.Cells[2,i])-StrToFloat(StringGrid1.Cells[2,i-GradSize.Value]))/(SampleRate.Value*GradSize.Value));
    end;
    StringGrid1.ColCount:=StringGrid1.ColCount+1;
    StringGrid1.Cells[(StringGrid1.ColCount-1),0]:='AlarmGrad';
    StringGrid1.ColCount:=StringGrid1.ColCount+1;
    StringGrid1.Cells[(StringGrid1.ColCount-1),0]:='AlarmTemp';
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var i: integer;
    cnt: integer;
    GRADCOLVal: integer;
    GRADCondVal: integer;
    TEMPCondVal: integer;
    GRADSampleRateVal: integer;
begin
     if StringGrid1.ColCount = 1 then exit;
     if StringGrid1.RowCount = 1 then exit;
     if (TermopileFile) then
     begin
          GRADCOLVal := GRADCOLSample2;
          GRADCondVal := GRADCondition2;
          TEMPCondVal := TEMPCondition2;
          GRADSampleRateVal := GRADSampleRate2;
     end
     else
     begin
          GRADCOLVal := GRADCOLSample;
          GRADCondVal := GRADCondition;
          TEMPCondVal := TEMPCondition;
          GRADSampleRateVal := GRADSampleRate;
     end;

     for i := 1 to StringGrid1.RowCount-1 do
     begin
          StringGrid1.Cells[GRADCOLVal,i]:='';
     end;
     for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
     begin
//          StringGrid1.Cells[GRADCOLVal,i]:=FloatToStr((StrToFloat(StringGrid1.Cells[1,i])-StrToFloat(StringGrid1.Cells[1,i-GradSize.Value]))/GradSize.Value)
          StringGrid1.Cells[GRADCOLVal,i]:=FloatToStr((StrToFloat(StringGrid1.Cells[2,i])-StrToFloat(StringGrid1.Cells[2,i-GradSize.Value]))/GradSize.Value)
     end;

     for i := 1 to StringGrid1.RowCount-1 do
     begin
          StringGrid1.Cells[GRADSampleRateVal,i]:='';
     end;
     for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
     begin
          StringGrid1.Cells[GRADSampleRateVal,i]:=FloatToStr((StrToFloat(StringGrid1.Cells[2,i])-StrToFloat(StringGrid1.Cells[2,i-GradSize.Value]))/(SampleRate.Value*GradSize.Value));
     end;

     Chart1LineSeries1.Clear;
     Chart1LineSeries2.Clear;
     Chart1LineSeries3.Clear;
     for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
     begin
          Chart1LineSeries1.AddXY(StrToInt(StringGrid1.Cells[0,i]),StrToFloat(StringGrid1.Cells[GRADCOLVal,i]));
          Chart1LineSeries2.AddXY(StrToInt(StringGrid1.Cells[0,i]),StrToFloat(StringGrid1.Cells[2,i])/(StrToFloat(StringGrid1.Cells[2,1])+TempCond.Value));
     end;

     cnt := 0;
     for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
     begin
         if (StrToFloat(StringGrid1.Cells[GRADCOLVal,i]) > GradCond.Value) then
         cnt := cnt + 1
         else cnt := 0;
         if (cnt > GradCntRow.Value) then
         StringGrid1.Cells[GRADCondVal,i] := 'True'
         else StringGrid1.Cells[GRADCondVal,i] := 'False';
     end;
     cnt := 0;
     for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
     begin
         //if (StrToFloat(StringGrid1.Cells[2,i]) > (StrToFloat(StringGrid1.Cells[2,1])+TempCond.Value)) then
         if (StrToFloat(StringGrid1.Cells[2,i]) > (ObjectTemp.Value+TempCond.Value)) then
         cnt := cnt + 1
         else cnt := 0;
         if (cnt > TempCntRow.Value) then
         StringGrid1.Cells[TEMPCondVal,i] := 'True'
         else StringGrid1.Cells[TEMPCondVal,i] := 'False';
     end;

     AlarmIndication.Brush.Color:=clGreen;
     for i := GradSize.Value+1 to StringGrid1.RowCount-1 do
     begin
          if CheckBox1.Checked then
          begin
              if (StringGrid1.Cells[GRADCondVal,i] = 'True') then
              begin
                   AlarmIndication.Brush.Color:=clRed;
                   Chart1LineSeries3.AddXY(StrToInt(StringGrid1.Cells[0,i]),1);
              end
              else Chart1LineSeries3.AddXY(StrToInt(StringGrid1.Cells[0,i]),0);
          end
          else if CheckBox2.Checked then
          begin
              if (StringGrid1.Cells[TEMPCondVal,i] = 'True') then
              begin
                   AlarmIndication.Brush.Color:=clRed;
                   Chart1LineSeries3.AddXY(StrToInt(StringGrid1.Cells[0,i]),1);
              end
              else Chart1LineSeries3.AddXY(StrToInt(StringGrid1.Cells[0,i]),0);
          end
          else
          begin
              if (StringGrid1.Cells[TEMPCondVal,i] = 'True') and (StringGrid1.Cells[GRADCondVal,i] = 'True') then
              begin
                   AlarmIndication.Brush.Color:=clRed;
                   Chart1LineSeries3.AddXY(StrToInt(StringGrid1.Cells[0,i]),1);
              end
              else Chart1LineSeries3.AddXY(StrToInt(StringGrid1.Cells[0,i]),0);
          end;
     end;
end;

end.

